# Firstassignment

Create Module and Access them in other files  -pymodule.py
Create file accessing different modules in python

Connect to database-perform DML tasks - Insert, update, delete, select data -pydml.py, data.sql
Run all the DML and DDL commands on a database and on tables

Create Text files - Read, Write and append file, delete file in a directory - pytextdata.py
Read, Write, Append text files in python

Create Json File, Read the data in Json file, convert it to dataframe - pyjson.py
Perform tasks relating to JSON data in python

object oriented concepts - inheritance, polymorphism - pyfile.py
Create concepts of inheritanace , polymorphism using python
